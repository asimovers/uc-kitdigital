import { nodeResolve } from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";
import json from "@rollup/plugin-json";
import { folderInput } from "rollup-plugin-folder-input";
import commonjs from "@rollup/plugin-commonjs";
// import del from "rollup-plugin-delete";

import { readFileSync } from "fs";

const pkg = JSON.parse(
  readFileSync(new URL("./package.json", import.meta.url), "utf8")
);

const getBanner = () => {
  const actualDate = new Date();
  return `/*! ${pkg.name} v${pkg.version} | ${
    pkg.author
  } - ${actualDate.getUTCFullYear()}  */ `;
};

const terserConfig = {
  compress: true,
  mangle: true,
};

const configCdnModern = {
  input: ["src/vanilla-components/uc-kitdigital.js"],
  output: {
    banner: getBanner,
    dir: "dist/modern/js",
    entryFileNames: `[name].js`,
    chunkFileNames: `[name].js`,
    assetFileNames: `[name].[ext]`,
    format: "es",
  },
  plugins: [json(), terser(terserConfig), nodeResolve()],
};

const configCdnIife = {
  input: ["src/vanilla-components/uc-kitdigital.js"],
  output: {
    dir: "dist/js",
    format: "iife",
    inlineDynamicImports: true,
    banner: getBanner,
  },
  plugins: [json(), nodeResolve({}), terser(terserConfig)],
};

const configModulesES = {
  input: "./src/vanilla-components/js/**/*.js",
  output: {
    dir: "dist/es",
    format: "es",
    chunkFileNames: "[name].js",
    banner: getBanner,
  },
  plugins: [nodeResolve(), folderInput(), json(), terser(terserConfig)],
};

const configModulesCJS = {
  input: "./src/vanilla-components/js/**/*.js",
  output: {
    chunkFileNames: "[name].js",
    dir: "dist/cjs",
    format: "cjs",
  },
  plugins: [
    commonjs(),
    nodeResolve(),
    folderInput(),
    json(),
    terser(terserConfig),
  ],
};

const configModulesSystem = {
  input: "./src/vanilla-components/js/**/*.js",
  output: {
    dir: "dist/system",
    format: "system",
    sourcemap: true,
  },
  plugins: [nodeResolve(), folderInput(), json(), terser(terserConfig)],
};

// const configModulesGlobalsES = {
//   input: "./src/vanilla-components/globals/**/*.js",
//   output: {
//     dir: "dist/es/globals",
//     format: "es",
//     chunkFileNames: "[name].js",
//     banner: getBanner,
//   },
//   plugins: [nodeResolve(), folderInput(), json(), terser(terserConfig)],
// };

// const configModulesGlobalsCJS = {
//   input: "./src/vanilla-components/globals/**/*.js",
//   output: {
//     chunkFileNames: "[name].js",
//     dir: "dist/cjs/globals",
//     format: "cjs",
//   },
//   plugins: [
//     commonjs(),
//     nodeResolve(),
//     folderInput(),
//     json(),
//     terser(terserConfig),
//   ],
// };

// const configModulesGlobalsSystem = {
//   input: "./src/vanilla-components/globals/*.js",
//   output: {
//     dir: "dist/system/globals",
//     format: "system",
//     sourcemap: true,
//   },
//   plugins: [nodeResolve(), folderInput(), json(), terser(terserConfig)],
// };

export default [
  configCdnModern,
  configCdnIife,
  configModulesES,
  configModulesCJS,
  configModulesSystem,
  // configModulesGlobalsES,
  // configModulesGlobalsCJS,
  // configModulesGlobalsSystem,
];
