/* eslint-disable no-console */
const fs = require("fs");
const ejs = require("ejs");
const chokidar = require("chokidar");
const dirTree = require("directory-tree");
const mkdirp = require("mkdirp");
const pkg = require("./package.json");

const renderIndex = (watch = false) => {
  // write a json with content of dirTree(`./src/vanilla-components/docs`)
  fs.writeFile(
    `./pages.json`,
    JSON.stringify(dirTree(`./src/vanilla-components/docs`), null, 2),
    (errToWrite) => {
      if (errToWrite) {
        // eslint-disable-next-line no-console
        console.error("Error to write file", errToWrite);
        return false;
      }
      return true;
    }
  );

  chokidar
    .watch("./web/indexView.ejs", {
      persistent: watch,
    })
    .on("all", () => {
      ejs.renderFile(
        `./web/indexView.ejs`,
        {
          documentTreeFolder: dirTree(`./src/vanilla-components/docs`),
          version: pkg.version,
        },
        async (err, html) => {
          fs.writeFile(`./dist/index.html`, html, (errToWrite) => {
            if (errToWrite) {
              // eslint-disable-next-line no-console
              console.error("Error to write file", errToWrite);
              return false;
            }
            return true;
          });
        }
      );
    });
};

const renderLocalViewsWatcher = (watch = false) => {
  const folderPath = (path) => path.split("/").slice(0, -1).join("/");
  chokidar
    .watch("./src/vanilla-components/docs", {
      persistent: watch,
    })
    .on("all", (event, path) => {
      if (path.includes("docs") && path.endsWith(".html")) {
        console.log("writting", path);
        ejs.renderFile(
          "./web/localView.ejs",
          {
            pathFile: path,
            version: pkg.version,
            removeContainer:
              process.argv.includes("-rc") ||
              process.argv.includes("--remove-container"),
          },
          async (err, html) => {
            await mkdirp(folderPath(`./dist/${path}`));
            fs.writeFile(`./dist/${path}`, html, (errToWrite) => {
              if (errToWrite) {
                console.log("Error to write file", errToWrite);
                return false;
              }
              return true;
            });
          }
        );
      }
    });
};

module.exports = {
  renderIndex,
  renderLocalViewsWatcher,
};
