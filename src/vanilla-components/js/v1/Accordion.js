export class Accordion {
  constructor(element, key) {
    if (element.tagName === "A") {
      const button = document.createElement("button");
      button.classList = element.classList;
      button.innerHTML = element.innerHTML;
      button.setAttribute(
        "data-collapse",
        element.getAttribute("data-collapse")
      );

      element.parentNode.insertBefore(button, element);
      element.remove();

      element = button;
    }
    this.element = element;
    this.key = key;
    window.accordion_prev = 0;
    this.list_class = {
      titleClass: "js-accordion-title",
      contentClass: "js-accordion-content",
      active_element: "js-accordion-active-element",
    };
  }

  mount() {
    this.element.classList.add(this.list_class.titleClass);
    this.element.addEventListener("click", (e) => this.onClick(e));
    this.closeAll();
    this.setUpTabIndexes();
    this.addEventListenerToHeadings();
  }

  unMount() {
    this.element.removeEventListener("click", this.onClick);
    this.element.classList.remove(this.list_class.titleClass);
    const content = this.element.nextElementSibling;
    content.classList.remove(this.list_class.contentClass);
    content.removeAttribute("style");
    content.removeAttribute("data-open");
  }

  getParent(element) {
    if (
      typeof element.parentElement === "undefined" ||
      element.parentElement === null
    )
      return null;

    if (typeof element.parentElement.dataset.accordion !== "undefined")
      return element.parentElement;

    return this.getParent(element.parentElement);
  }

  onClick(e) {
    const d = +new Date();
    const secDiff = (d - window.accordion_prev) / 1000;
    if (secDiff < 0.2) {
      return;
    }
    window.accordion_prev = d;
    const element = e.target;

    const parent = this.getParent(element);

    if (parent == null) {
      const targets = document.querySelectorAll(
        `[data-toggle="${element.dataset.collapse}"]`
      );
      targets.forEach((item) => {
        this.toggle(item, element);
      });
      return;
    }

    const targets = parent.querySelectorAll(
      `[data-toggle="${element.dataset.collapse}"]`
    );

    if (element.dataset.open === "true") {
      this.toggle(element, element);
      return;
    }

    /* SALVANDO ESTADOS */

    this.closeAll(parent, targets);

    targets.forEach((item) => {
      this.toggle(item, element);
    });

    // append raw html to body
  }

  closeAll(parent, objectives) {
    const isFirst = typeof parent === "undefined";
    const container = isFirst ? document : parent;
    const headers = container.querySelectorAll(".uc-collapse_heading");
    const targets = container.querySelectorAll(
      ".uc-collapse_body[data-toggle]"
    );

    targets.forEach((item) => {
      if (typeof objectives === "undefined") {
        this.operate(item, isFirst, container);
        return;
      }

      objectives.forEach((obj) => {
        if (obj === item) {
          return;
        }
        this.operate(item, isFirst, container);
      });
    });

    headers.forEach((item) => {
      this.revertSeeMoreToDefault(item);
    });
  }

  operate(item, isFirst, container) {
    if (isFirst) {
      item.classList.add(this.list_class.contentClass);
      if (
        typeof item.dataset.open !== "undefined" &&
        item.dataset.open !== "false"
      ) {
        this.toggle(item);
        return;
      }
    }
    item.dataset.open = false;
    item.style.height = 0;

    const parent = container.querySelectorAll(
      `[data-collapse="${item.dataset.toggle}"]`
    );

    if (typeof parent[0] !== "undefined") {
      parent.forEach((par) => {
        par.classList.remove(this.list_class.active_element);
      });
      return;
    }

    if (typeof parent.classList === "undefined") {
      return;
    }

    parent.classList.remove(this.list_class.active_element);
  }

  setUpTabIndexes() {
    const titleElements = document.querySelectorAll(".js-accordion-title");
    titleElements.forEach((item) => {
      item.setAttribute("tabindex", 0);
    });
    const contentElements = document.querySelectorAll(".js-accordion-content");
    contentElements.forEach((item) => {
      this.handleTabIndexes(item, -1);
    });
  }

  addEventListenerToHeadings() {
    const titleElements = document.querySelectorAll(".js-accordion-title");

    titleElements.forEach((item) => {
      item.addEventListener("keydown", (e) => {
        if (e.keyCode === 13 || e.keyCode === 32) {
          e.preventDefault();
          this.onClick(e);
        }
      });
    });
  }

  // eslint-disable-next-line class-methods-use-this
  revertSeeMoreToDefault(element) {
    const text = element.textContent;
    const regex = /ver menos/gi;
    const match = text.match(regex);
    const contains = match && match.length > 0;

    if (contains) {
      const elementsToScan = element.querySelectorAll("span, div, p");
      const hasSeeLess = Array.from(elementsToScan).filter((span) =>
        regex.test(span.textContent)
      );
      if (hasSeeLess.length > 0) {
        const seeMore = hasSeeLess.reduce((prev, current) =>
          prev.textContent.length < current.textContent.length ? prev : current
        );
        seeMore.innerHTML = seeMore.innerHTML.replace("Ver menos", "Ver más");
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  toggleSeeMoreText(element) {
    // console.log(element.textContent.includes('ver mas')); do it with a regex
    const text = element.textContent;
    // does text contain "ver mas"? with regex
    const regex = /ver m[aá]s/gi;
    const match = text.match(regex);
    const contains = match && match.length > 0;

    if (contains) {
      const elementsToScan = element.querySelectorAll("span, div, p");
      const hasSeeMore = Array.from(elementsToScan).filter((span) =>
        regex.test(span.textContent)
      );
      if (hasSeeMore.length > 0) {
        const seeMore = hasSeeMore.reduce((prev, current) =>
          prev.textContent.length < current.textContent.length ? prev : current
        );
        seeMore.innerHTML = seeMore.innerHTML.replace("Ver más", "Ver menos");
      }
    }
  }

  // eslint-disable-next-line class-methods-use-this
  handleTabIndexes(element, value) {
    const elements = element.querySelectorAll("a, button, input, select");
    elements.forEach((item) => {
      item.setAttribute("tabindex", value);
    });
  }

  // eslint-disable-next-line class-methods-use-this
  rotateIcon(element) {
    const icon = element.querySelector(".uc-icon.icon-shape--rounded");
    if (icon) {
      icon.classList.toggle("uc-icon--rotate-180");
    }
  }

  toggle(el, parent) {
    // getting the height every time in case
    // the content was updated dynamically

    const height = el.scrollHeight;

    if (el.style.height === "0px" || el.style.height === "") {
      el.style.height = `${height}px`;
      el.setAttribute("data-open", "true");
      this.setUpTabIndexes();
      this.handleTabIndexes(el, 0);
      this.rotateIcon(parent);
      this.toggleSeeMoreText(parent);
      if (typeof parent !== "undefined") {
        parent.classList.add(this.list_class.active_element);
      }
    } else {
      el.style.height = 0;
      el.setAttribute("data-open", "false");
      this.setUpTabIndexes();
      this.handleTabIndexes(el, -1);
      this.rotateIcon(parent);
      if (typeof parent !== "undefined") {
        parent.classList.remove(this.list_class.active_element);
      }
    }
  }
}
