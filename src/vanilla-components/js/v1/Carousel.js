import { tns } from "tiny-slider/src/tiny-slider";

// eslint-disable-next-line import/prefer-default-export
export class Carousel {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.carrouselRoot = this.element;
    this.autoPlayButtons = [
      "<i class='uc-icon play-button'>play_circle_filled</i>",
      "<i class='uc-icon pause-button'>pause_circle_filled</i>",
    ];
    this.carrouselOptions = null;
  }

  mount() {
    if (this.carrouselRoot.getElementsByClassName("tns-outer").length === 1)
      return;

    const data = this.carrouselRoot.dataset;

    this.carrouselOptions = {
      container: `.${data.carousel}`,
      items: 1,
      slideBy: data.slideby ? Number(data.slideby, 10) : 1,
      loop: data.loop ? JSON.parse(data.loop) : false,
      nav: data.nav ? JSON.parse(data.nav) : false,
      gutter: data.gutter ? Number(data.gutter) : 10,
      autoplay: data.autoplay ? JSON.parse(data.autoplay) : false,
      speed: data.speed ? Number(data.speed, 10) : 300,
      controls: data.controls ? JSON.parse(data.controls) : false,
      navContainer: data.navcontainer ? data.navcontainer : false,
      autoplayButton: data.autoplaybutton ? data.autoplaybutton : false,
      prevButton: data.prevButton ? data.prevButton : false,
      nextButton: data.nextButton ? data.nextButton : false,
      autoplayText: data.autoplayText ? this.autoPlayButtons : false,
      autoplayTimeout: data.autoplayTimeout
        ? Number(data.autoplayTimeout)
        : 5000,
      responsive: {
        // 320: {
        //   controls: isCardCarousel && !(totalItems === 1),
        // },
        768: {
          items: data.smItems ? Number(data.smItems) : 1,
          gutter: 32,
          // controls: isCardCarousel && !(Number(data.smItems) > totalItems),
        },
        992: {
          items: data.items ? Number(data.items) : 1,
          //   controls: isCardCarousel && !(Number(data.items) > totalItems),
        },
      },
    };

    const carousel = tns(this.carrouselOptions);

    if (this.carrouselRoot === undefined) return;
    this.carrouselRoot.style.opacity = "1";

    if (data.counter && JSON.parse(data.counter)) {
      const indexCurrent =
        this.carrouselRoot.getElementsByClassName("index-current")[0];
      const indexTotal =
        this.carrouselRoot.getElementsByClassName("index-total")[0];
      const itemOffSet = data.offset ? Number(data.offset) : 0;
      indexCurrent.innerHTML = carousel.getInfo().items - itemOffSet;
      indexTotal.innerHTML = carousel.getInfo().slideCount - itemOffSet;
      carousel.events.on("indexChanged", (info) => {
        indexCurrent.innerHTML = info.items - itemOffSet + info.index;
      });
    }
    if (data.track && JSON.parse(data.track)) {
      const containerWidth =
        document.getElementsByClassName("container")[0].offsetWidth;
      const slideSegment = containerWidth / carousel.getInfo().slideCount;
      const carouselTrack = this.carrouselRoot.getElementsByClassName(
        "carousel-progress-indicator"
      )[0];
      carousel.events.on("indexChanged", (info) => {
        const totalpx = slideSegment * info.index;
        carouselTrack.style.transform = `translateX(${totalpx}px)`;
      });
    }

    // progressBar = the element by class name uc-carousel__progress-bar
    const progressBar = this.carrouselRoot.getElementsByClassName(
      "uc-carousel__progress-bar"
    )[0];

    let savedTimeout;
    let preventStart = false;
    let counter = 0;

    const amountOfItems = this.carrouselRoot.children[0].children.length; // 3
    function startProgressBar() {
      if (counter > amountOfItems) {
        counter = 0;
        return;
      }
      if (preventStart) return;
      progressBar.style = ` --duration: 0ms; --percentage: 0%;`;
      savedTimeout = setTimeout(() => {
        progressBar.style = ` --duration: ${
          counter === 0
            ? this.carrouselOptions.autoplayTimeout - 500
            : this.carrouselOptions.autoplayTimeout - 200
        }ms; --percentage: 100%;`;
        counter++;
      }, 100);
    }

    if (progressBar) {
      startProgressBar();
      carousel.events.on("indexChanged", startProgressBar);

      const pauseBtn =
        this.carrouselRoot.getElementsByClassName("autoplay-buttons")[0];

      pauseBtn.addEventListener("click", () => {
        if (pauseBtn.dataset.action === "stop") {
          startProgressBar();
          return;
        }
        progressBar.style = ` --duration: 2000ms; --percentage: 999%;`;
      });
      const carouselNav =
        this.carrouselRoot.getElementsByClassName("carousel-nav-list")[0];

      carouselNav.addEventListener("mouseenter", () => {
        preventStart = true;
      });
      carouselNav.addEventListener("mouseleave", () => {
        preventStart = false;
      });
      carouselNav.addEventListener("click", () => {
        clearTimeout(savedTimeout);
        progressBar.style = ` --duration: 2000ms; --percentage: 999%;`;
      });
    }

    // compensa ancho de scroll track, usando detector de ancho de scrollbar de David Walsh https://davidwalsh.name/detect-scrollbar-width

    function getScrollBarWidth() {
      const scrollDiv = document.createElement("div");
      scrollDiv.className = "scrollbar-measure";
      document.body.appendChild(scrollDiv);
      const scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
      return scrollbarWidth;
    }

    if (data.type === "cards") {
      this.carrouselRoot.style.width = `calc(100vw - ${
        getScrollBarWidth() / 2
      }px)`;
    }
  }
}
