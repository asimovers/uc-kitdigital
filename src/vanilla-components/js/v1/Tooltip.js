import tippy from "tippy.js";

// eslint-disable-next-line import/prefer-default-export
export class Tooltip {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.arrow = true;
    this.arrowType = "round";
    this.interactive = true;
  }

  mount() {
    tippy(this.element, {
      theme: "uc",
      arrow: this.arrow,
      arrowType: this.arrowType,
      interactive: this.interactive,
    });
  }
}
