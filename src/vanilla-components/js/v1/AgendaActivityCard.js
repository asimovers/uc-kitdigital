import dayjs from "dayjs/esm";
import localizedFormat from "dayjs/esm/plugin/localizedFormat";
import customParseFormat from "dayjs/esm/plugin/customParseFormat";
import utc from "dayjs/esm/plugin/utc";
import weekday from "dayjs/esm/plugin/weekday";
import localeData from "dayjs/esm/plugin/localeData";
import isBetween from "dayjs/esm/plugin/isBetween";
import "dayjs/esm/locale/es";

dayjs.extend(isBetween);
dayjs.extend(weekday);
dayjs.extend(localeData);
dayjs.extend(utc);
dayjs.extend(localizedFormat);
dayjs.extend(customParseFormat);

dayjs.locale("es");

const CHILE_SUMMER_OFFSET = -3; // DST offset
const CHILE_WINTER_OFFSET = -4; // Standard offset

// DST change ranges
const NO_DST_RANGES = {
  2023: {
    start: dayjs.utc("2023-04-06T03:00:00Z"),
    end: dayjs.utc("2023-09-07T04:00:00Z"),
  },
  2024: {
    start: dayjs.utc("2024-04-07T03:00:00Z"),
    end: dayjs.utc("2024-09-08T04:00:00Z"),
  },
  2025: {
    start: dayjs.utc("2025-04-06T03:00:00Z"),
    end: dayjs.utc("2025-09-07T04:00:00Z"),
  },
  2026: {
    start: dayjs.utc("2026-04-05T03:00:00Z"),
    end: dayjs.utc("2026-09-06T04:00:00Z"),
  },
};

const dayjsChile = (date) => {
  const utcDate = dayjs.utc(date);

  let offset = CHILE_SUMMER_OFFSET;

  // Check if the date falls within the winter time (no DST) and if so, adjust the offset
  Object.keys(NO_DST_RANGES).forEach((year) => {
    const range = NO_DST_RANGES[year];
    if (utcDate.isAfter(range.start) && utcDate.isBefore(range.end)) {
      offset = CHILE_WINTER_OFFSET;
    }
  });

  return utcDate.utcOffset(offset, false);
};

export class Card {
  makeCard(activity) {
    const month = dayjsChile(activity.nextDate.start).format("MMMM");
    const day = dayjsChile(activity.nextDate.start).format("D");

    const shortMonth = dayjsChile(activity.nextDate.start).format("MMM");

    function getDate() {
      const date = dayjsChile(activity.nextDate.start).format("DD");
      return `${date} de ${month}`;
    }

    function getHours() {
      const start = dayjsChile(activity.nextDate.start).format("HH:mm");
      const end = dayjsChile(activity.nextDate.end).format("HH:mm");
      if (start === "00:00" && end === "23:59") return "Todo el día";
      return `${start} a ${end} hrs`;
    }

    const imgResolver = (img) => {
      if (img.formats.small) return img.formats.small.url;
      return img.url;
    };
    let img;
    if (this.hideImg) {
      img = "";
    } else {
      img = `
        <img
          src="${imgResolver(activity.mainImage)}"
          class="d-none d-lg-block img-fluid"
          loading="lazy"
          height="100"
          width="100%"
        >
      `;
    }

    let tags = "";
    if (this.hideTag || !activity.activityTypes.length) {
      tags = "";
    } else {
      tags = `
          <a
            href="https://uc.cl/agenda/?activityTypes=${activity.activityTypes[0].id}"
            class="uc-tag mt-20"
          >
            ${activity.activityTypes[0].name}
          </a>
        `;
    }

    return `
      <div class="uc-card card-type--event card-height--same ${
        this.hideImg ? "" : ""
      }">
        ${img}
        <div class="uc-card_body d-flex">
          ${tags}
          <div class="d-flex h-full flex-column">
            <a
              href=
              "${this.base_url ? this.base_url : "https://uc.cl/agenda"}/${
      activity.slug
    }"
              ${this.base_url ? "" : 'target="_blank"'}
              class="d-flex h4 color-black uc-card-link my-16
              title="${activity.title}">
                ${activity.title}
            </a>
          </div>
          <div>
            <div class="uc-card card-type--date ${
              this.middleDate ? "middle-date" : ""
            }">
              <div class="day">
                <span>${day}</span>
              </div>
              <div class="month">
                <span>${shortMonth}</span>
              </div>
            </div>
            <div class="uc-card_event--content">
              <div class="date">
                <span>${getDate()}</span>
              </div>
              <div class="time">${getHours()}</div>
        ${
          activity.place
            ? `<div class="venue">
        <p class="no-margin">${activity.place.name}</p>
      </div>`
            : ""
        }
            </div>
            </div>
        </div>
      </div>
    `;
  }
}
