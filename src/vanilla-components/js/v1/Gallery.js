import PhotoSwipeLightbox from "photoswipe/lightbox";

export class Gallery {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.galleryClass = ".uc-gallery";
    const lightbox = new PhotoSwipeLightbox({
      gallery: this.galleryClass,
      children: "a",
      pswpModule: () => import("photoswipe"),
      padding: { top: 50, bottom: 50, left: 50, right: 50 },
      mainClass: "uc-gallery",
    });

    lightbox.on("uiRegister", function () {
      lightbox.pswp.ui.registerElement({
        name: "bulletsIndicator",
        className: "pswp__bullets-indicator",
        appendTo: "wrapper",
        onInit: (el, pswp) => {
          const bullets = [];
          let bullet;
          let prevIndex = -1;

          for (let i = 0; i < pswp.getNumItems(); i++) {
            bullet = document.createElement("div");
            bullet.className = "pswp__bullet";
            bullet.onclick = (e) => {
              pswp.goTo(bullets.indexOf(e.target));
            };
            el.appendChild(bullet);
            bullets.push(bullet);
          }

          pswp.on("change", (a) => {
            if (prevIndex >= 0) {
              bullets[prevIndex].classList.remove("pswp__bullet--active");
            }
            bullets[pswp.currIndex].classList.add("pswp__bullet--active");
            prevIndex = pswp.currIndex;
          });
        },
      });
      lightbox.pswp.ui.registerElement({
        name: "custom-caption",
        order: 9,
        isButton: false,
        appendTo: "root",
        html: "Caption text",
        onInit: (el, pswp) => {
          lightbox.pswp.on("change", () => {
            const currSlideElement = lightbox.pswp.currSlide.data.element;
            let captionHTML = "";
            if (currSlideElement) {
              captionHTML = currSlideElement
                .querySelector("img")
                .getAttribute("title");
            }
            if (!captionHTML) {
              el.classList.add("hidden-caption-content");
            } else {
              el.classList.remove("hidden-caption-content");
            }
            el.innerHTML = captionHTML || "";
          });
        },
      });
    });
    this.lightbox = lightbox;
  }

  mount() {
    this.lightbox.init();
  }
}
