export class BackgroundVideo {
  constructor(element, key) {
    this.key= key;
    this.element = element;
    this.element = null;
    this.video = null;
    this.videoId = null;
    this.control = null;
    this.innerHtmlPause = null;
    this.innerHtmlPlay = null;
    this.togglePlayback = this.togglePlayback.bind(this);
  }

  mount() {

    this.video = this.element;

    this.videoId = this.video.getAttribute("data-bg-video-id");
    this.control = document.querySelector(
      `[data-bg-video-control-id="${this.videoId}"]`
    );

    this.control.classList.add("uc-background-video__control");
    this.control.setAttribute("data-bg-video-overlay-id", this.videoId);
    this.control.setAttribute("tabIndex", 0);

    this.innerHtmlPause = `<i class="uc-icon icon-shape--rounded icon-background--white">pause</i><span>Pausar</span>`;
    this.innerHtmlPlay = `<i class="uc-icon icon-shape--rounded icon-background--white">play_arrow</i><span>Reproducir</span>`;
    this.control.innerHTML = this.innerHtmlPause;

    this.control.addEventListener("click", this.togglePlayback);

    this.video.addEventListener("pause", () => {
      this.control.innerHTML = this.innerHtmlPlay;
    });

    this.video.addEventListener("play", () => {
      this.control.innerHTML = this.innerHtmlPause;
    });
  }

  togglePlayback() {
    if (!this.video) {
      return;
    }
    if (this.video.paused || this.video.ended) {
      if (this.video.currentTime === this.video.duration) {
        this.video.currentTime = 0;
      }
      this.video.play();
      // control.innerHTML = innerHtmlPause;
    } else {
      this.video.pause();
      // control.innerHTML = innerHtmlPlay;
    }
  }
}
