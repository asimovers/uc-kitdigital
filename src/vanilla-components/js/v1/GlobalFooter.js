export class GlobalFooter {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.id = null;
    this.validIds = ["uc-global-footer", "uc-global-footer-en"];
  }

  async mount() {
    // get the element.id  from the element

    this.id = this.element.id;
    if (!this.validIds.includes(this.id)) return;

    if (this.id === "uc-global-footer") this.renderGlobalFooterHtml();
    else if (this.id === "uc-global-footer-en")
      this.renderEnglishGlobalFooterHtml();
    else throw new Error("Invalid Footer id");
  }

  // #uc-global-footer
  renderGlobalFooterHtml() {
    this.element.innerHTML = `<div class="uc-footer_help">
    <div class="container">
      <div class="row">
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">Mesa central</div>
          <p class="p-size--sm">
            Teléfono para comunicarse con las distintas áreas de la Universidad.
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">phone</i>
            <a href="tel:56955044000">(56)95504 4000</a>
          </p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">emergencias uc</div>
          <p class="p-size--sm">
            Teléfono en caso de accidente o situación que ponga en riesgo tu vida
            dentro de algún campus.
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">phone</i>
            <a href="tel:56955045000">(56)95504 5000</a>
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">launch</i>
            <a href="https://www.uc.cl/emergencias" class="" target="_blank"
              >Ir al sitio de Emergencias</a
            >
          </p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">Discriminación y Violencia</div>
          <p class="p-size--sm">
            Orientación y apoyo en casos de discriminación, violencia de género o
            violencia sexual.
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">phone</i>
            <a href="tel:800 001 222">Fonoayuda: 800 001 222</a>
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">launch</i>
            <a href="https://noalaviolenciasexual.uc.cl" target="_blank" class=""
              >Más ayuda</a
            >
          </p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">Mediación Universitaria</div>
          <p class="p-size--sm">
            Teléfonos para orientación y consejo si se ha vulnerado alguno de tus
            derechos en la universidad.
          </p>
          <p class="p-size--sm mb-8">
            <i class="uc-icon">phone</i>
            <a href="tel:56955041691">(56)95504 1691</a>
          </p>
          <p class="p-size--sm">
            <i class="uc-icon">phone</i>
            <a href="tel:56955041247" class="uc-btn btn-inline">(56)95504 1247</a>
          </p>
          <p class="p-size--sm">
            <i class="uc-icon mr-4">launch</i
            ><a href="https://ombuds.uc.cl/" target="_blank" class=""
              >Ir a la Oficina de Ombuds UC</a
            >
          </p>
        </div>
      </div>
    </div>
  </div>`;
  }

  // #uc-global-footer-en
  renderEnglishGlobalFooterHtml() {
    this.element.innerHTML = `<div class="uc-footer_help">
    <div class="container">
      <div class="row">
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">Main Phone</div>
          <p class="p-size--sm">Number to connect to all UC offices (in Spanish).</p>
          <p class="p-size--sm"><i class="uc-icon">phone</i> <a href="tel:56955044000">(56)95504 4000</a> (ES)</p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">UC Emergency Line</div>
          <p class="p-size--sm">In case of an accident or a situation that puts your life in danger at one of our campuses (in Spanish).</p>
          <p class="p-size--sm"><i class="uc-icon">phone</i> <a href="tel:56955045000">(56)95504 5000</a> (ES)</p>
          <p class="p-size--sm"><i class="uc-icon">launch</i> <a href="https://www.uc.cl/emergencias" class="" target="_blank">Emergency Site (ES)</a></p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">Sexual Violence and Discrimination Line</div>
          <p class="p-size--sm">For guidance and support in cases of discrimination, gender violence or sexual violence (in Spanish).</p>
          <p class="p-size--sm"><i class="uc-icon">phone</i> <a href="tel:800 001 222">Help line: 800 001 222</a> (ES)</p>
          <p class="p-size--sm"><i class="uc-icon">launch</i> <a href="https://noalaviolenciasexual.uc.cl" target="_blank" class="">More help</a> (ES)</p>
        </div>
        <div class="col-lg mb-12">
          <div class="uc-footer_list-title mb-8">University Ombuds Office</div>
          <p class="p-size--sm">If you feel your rights have been violated at the University, please contact the UC Ombuds Office (in Spanish) for guidance and advice.</p>
          <p class="p-size--sm mb-8"><i class="uc-icon">phone</i> <a href="tel:56955041691">(56)95504 1691</a> (ES)</p>
          <p class="p-size--sm"><i class="uc-icon">phone</i> <a href="tel:56955041247" class="uc-btn btn-inline">(56)95504 1247</a> (ES)</p>
          <p class="p-size--sm"><i class="uc-icon mr-4">launch</i><a href="https://ombuds.uc.cl/" target="_blank">UC Ombuds Office</a> (ES)</p>
        </div>
      </div>
    </div>
  </div>`;
  }
}
