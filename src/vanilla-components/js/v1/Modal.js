// eslint-disable-next-line import/prefer-default-export
export class Modal {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.modalOpen = null;
    this.modalElement = null;
    this.target = null;
  }

  mount() {
    this.element.addEventListener("click", () => this.open());
    this.target = this.element.dataset.mtarget;
    this.modalElement = document.querySelector(`[data-modal='${this.target}']`);
    this.setClosedButton(this.element);
  }

  open() {
    if (this.modalElement === null) return;
    this.modalElement.style.display = "block";
    this.modalOpen = this.modalElement;
  }

  close() {
    this.modalElement.style.display = "none";
    this.modalOpen = null;
  }

  setClosedButton() {
    const closedButton = this.modalElement.querySelector("[data-mclosed]");
    closedButton.addEventListener("click", () => this.close());
    const wClick = window.onclick;

    window.onclick = (event) => {
      if (typeof wClick === "function") wClick(event);

      if (event.target === this.modalOpen)
        this.modalOpen.style.display = "none";
    };

    document.addEventListener("keydown", (e) => {
      if (e.key === "Escape") {
        if (this.modalOpen === null) return;
        this.close();
      }
    });
  }
}
