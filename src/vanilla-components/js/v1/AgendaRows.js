import { Base } from "./AgendaBase";

export class AgendaRows extends Base {
  makeSkeleton() {
    let skeletons = `<div id="${this.uniqueId}" class="row">`;
    for (let index = 0; index < this.limit; index++) {
      skeletons += `
      <div class="col-sm-${this.sm} col-lg-${this.lg} col-md-${this.md} mb-32">
        <div class="uc-card uc-skeleton">
          ${
            !this.hideImg
              ? '<div class="uc-skeleton-item uc-skeleton_img"></div>'
              : ""
          }
          <div class="uc-card_body">
            <div class="uc-skeleton-item uc-skeleton_span my-20" placeholder="Texto tag">
            </div>
            <div class="uc-skeleton-item uc-skeleton_h4" placeholder="Este es el titulo del card de actividad de tres lineas">
            </div>
            <div class="d-flex mt-32">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
            <div class="d-flex mt-8">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
            <div class="d-flex mt-8">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
          </div>
        </div>
      </div>`;
    }
    return `${skeletons}</div>`;
  }

  async makeComponent() {
    const skeletonUI = this.getSkeletonUI();
    const empyStateUI = this.getEmptyStateUI();
    const errorUi = this.getErrorUI();

    this.el.innerHTML = skeletonUI;
    let data;

    try {
      data = await this.getData();
    } catch (e) {
      this.el.innerHTML = errorUi;
      return;
    }

    if (data.data.length === 0) {
      this.el.innerHTML = empyStateUI;
      return;
    }

    let html = "";
    Object.values(data.data).forEach((activity) => {
      html += `
      <div class="col-sm-${this.sm} col-md-${this.md} mb-32">
        ${this.makeCard(activity)}
      </div>`;
    });
    this.el.innerHTML = `<div class="row">${html}</div>`;
  }
}
