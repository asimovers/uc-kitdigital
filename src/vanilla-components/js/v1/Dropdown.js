// eslint-disable-next-line import/prefer-default-export
export class Dropdown {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.dropdownRoot = element;
    this.list_class = {
      button: "dropbtn",
      show: "show",
    };
    this.trigger = null;
  }

  mount() {
    this.trigger = this.dropdownRoot.querySelector("[data-dtarget]");

    if (this.dropdownRoot.getAttribute("data-initializated") === "true") return;
    this.trigger.addEventListener("click", (e) => this.onClick(e));

    this.prepareDropButtons();
    this.prepareClose();

    this.dropdownRoot.setAttribute("data-initializated", "true");
  }

  onClick(e) {
    const element = e.target;
    const target = this.dropdownRoot.querySelector(
      `[data-dropdown='${element.dataset.dtarget}']`
    );
    target.classList.toggle(this.list_class.show);
    e.preventDefault();
  }

  prepareDropButtons() {
    const dropdowns = this.dropdownRoot.querySelector("[data-dropdown]");

    if (dropdowns == null) {
      return;
    }

    if (!Array.isArray(dropdowns)) {
      dropdowns.classList.add(this.list_class.button);
      return;
    }

    dropdowns.forEach((element) => {
      element.classList.add(this.list_class.button);
    });
  }

  prepareClose() {
    const wClick = window.onclick;
    window.onclick = (event) => {
      if (typeof wClick === "function") {
        // eslint-disable-next-line no-restricted-globals
        wClick(event);
      }

      // evaluate if the target is inside the element with class "uc-ecosistema", if so, return
      if (event.target.closest("[data-close-on-click-outside]")) {
        return;
      }

      // eslint-disable-next-line no-restricted-globals
      if (!event.target.matches(`.${this.list_class.button}`)) {
        const dropdowns = this.dropdownRoot.querySelectorAll("[data-dropdown]");

        if (dropdowns == null) {
          return;
        }
        if (dropdowns.length === 1) {
          if (dropdowns[0].classList.contains(this.list_class.show)) {
            dropdowns[0].classList.remove(this.list_class.show);
          }
          return;
        }
        for (let i = 0; i < dropdowns.length; i++) {
          const openDropdown = dropdowns[i];
          if (openDropdown.classList.contains(this.list_class.show)) {
            openDropdown.classList.remove(this.list_class.show);
          }
        }
      }
    };
  }
}
