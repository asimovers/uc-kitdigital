import { createPopper } from "@popperjs/core/dist/esm/popper";

export class Navbar {
  constructor(element, key) {
    this.key = key;
    this.element = element;

    // navbar
    this.navbar = null;
    this.navbarDropdowns = null;

    // mobile Navbar
    this.mobileNavbar = null;
    this.toggleButton = null;

    // nav (Old, in desuse)
    this.nav = null;
    this.navElements = null;

    this.isNavbar = null;
    this.isMobileNavbar = null;
    this.isNav = null;
  }

  mount() {
    this.navbar = this.element;
    this.nav = this.element;
    this.mobileNavbar = this.element;

    this.isNavbar = this.element.classList.contains("uc-navbar_nav");
    this.isMobileNavbar = this.element.classList.contains(
      "navbar_mobile-slide"
    );
    this.isNav = this.element.classList.contains("nav");

    if (this.isNavbar) this.mountNavbar();
    else if (this.isMobileNavbar) this.mountMobileNavbar();
    else if (this.isNav) this.mountNav();
  }

  mountNavbar() {
    this.navbarDropdowns = this.navbar.querySelectorAll(".uc-navbar-dropdown");

    if (!this.navbar) return;

    if (!this.navbar || this.navbar.dataset.init) return;
    this.navbar.dataset.init = true;

    this.navbarDropdowns.forEach((element) => {
      const button = element;
      const submenu = element.querySelector(".uc-navbar-dropdown_menu");

      submenu.insertAdjacentHTML(
        "beforeend",
        '<li><div class="uc-navbar_arrow" data-popper-arrow></div></li>'
      );

      let popperInstance = null;

      function getContainerMaxWidth() {
        const width = window.innerWidth;

        if (width >= 1200) return 1248; // .container-xl
        if (width >= 992) return 960; // .container-lg
        if (width >= 768) return 720; // .container-md
        if (width >= 576) return 540; // .container-sm

        return width; // Default for smaller screens
      }

      function create() {
        popperInstance = createPopper(button, submenu, {
          placement: "bottom",
          modifiers: [
            {
              name: "preventOverflow",
              // options: {
              //   // navbarRoot: this.navbar,
              // },
            },
            {
              name: "flip",
              enabled: false,
            },
            {
              name: "offset",
              options: {
                offset: ({ placement, reference, popper }) => {
                  // Get container's max width based on current screen size
                  const containerMaxWidth = getContainerMaxWidth();

                  // Calculate the available space to the right of the reference element
                  const availableSpaceToRight =
                    containerMaxWidth - reference.x - reference.width;

                  // Calculate the horizontal offset to ensure the popper doesn't overflow the container
                  let horizontalOffset = 0;

                  if (
                    placement.startsWith("bottom") ||
                    placement.startsWith("top")
                  ) {
                    if (availableSpaceToRight < popper.width) {
                      horizontalOffset = popper.width - availableSpaceToRight;
                    }
                  }

                  // Return the calculated offset
                  return [horizontalOffset, 10];
                },
              },
            },
          ],
        });
      }
      function destroy() {
        if (popperInstance) {
          popperInstance.destroy();
          popperInstance = null;
        }
      }

      function show() {
        submenu.setAttribute("data-show", "");
        create();
      }

      function hide() {
        submenu.removeAttribute("data-show");
        destroy();
      }

      // function tabNav(e) {
      //   if (e.key === "Tab" && !el.contains(document.activeElement)) {
      //     hide();
      //   }
      // }

      const navButtonRow = Array.from(
        this.navbar.querySelectorAll(".nav-item>.uc-btn, .nav-item>a[href]")
      );
      // const submenuItems = Array.from(
      //    this.navbar.querySelectorAll(
      //     '[data-show] a[href], [data-show] button, [data-show] [tabindex]:not([tabindex="-1"])'
      //   )
      // );
      function findNextFocusableElement(current, focusableElements) {
        // Obtener una lista de todos los elementos focuseables

        // Encontrar el elemento actualmente enfocado
        // console.log(focusableElements);
        const currentElement = current;
        const currentIndex = focusableElements.indexOf(currentElement);

        // Calcular el índice del próximo elemento enfocable
        const nextIndex = (currentIndex + 1) % focusableElements.length;

        // Enfocar el próximo elemento
        const nextElement = focusableElements[nextIndex];
        nextElement.focus();
      }

      // the same but previous
      function findPreviousFocusableElement(current, focusableElements) {
        const currentElement = current;
        const currentIndex = focusableElements.indexOf(currentElement);

        const previousIndex =
          (currentIndex - 1 + focusableElements.length) %
          focusableElements.length;

        const previousElement = focusableElements[previousIndex];
        previousElement.focus();
      }

      submenu.addEventListener("keydown", (e) => {
        if (e.key === "Escape") {
          hide();
          button.querySelector("a, button").focus();
        }
      });

      button.addEventListener("keydown", (e) => {
        if (e.key === "Escape") hide();
      });

      this.navbar.addEventListener("keydown", (e) => {
        // is the last element in the submenu
        // si e.target esta en navButtonRow
        if (navButtonRow.includes(e.target)) {
          if (e.key === "ArrowRight") {
            e.preventDefault();
            const next = findNextFocusableElement(e.target, navButtonRow);
            if (next) next.focus();
          }
          if (e.key === "ArrowLeft") {
            e.preventDefault();
            const prev = findPreviousFocusableElement(e.target, navButtonRow);
            if (prev) prev.focus();
            else this.navbar.lastElementChild.focus();
          }
        }

        if (e.key === " ") {
          // toma el enlace si es un link y abrelo con JS
          if (e.target.tagName === "A") {
            e.preventDefault();
            e.target.click();
          }

          // e.preventDefault();
          // e.target.click();
        }
      });

      // on focus, open the current submenu and close any other open submenu
      button.addEventListener("focus", show);
      // open the current submenu and close any other open submenu
      button.addEventListener("click", () => {
        if (button.tagName === "A") return;

        if (submenu.hasAttribute("data-show")) {
          hide();
        } else {
          show();
        }
      });

      // document.addEventListener("keyup", tabNav);
      const showEvents = ["mouseenter", "focus"];

      showEvents.forEach((event) => {
        button.addEventListener(event, show);
      });

      button.addEventListener("mouseleave", hide);

      function menuWidth(liEl, navbar) {
        const items = liEl.querySelectorAll("li").length;

        if (items > 60) {
          liEl.style.columnCount = "5";
          liEl.style.width = `${navbar.offsetWidth}px`;
        } else if (items > 50) {
          liEl.style.columnCount = "4";
          liEl.style.width = `${navbar.offsetWidth}px`;
        } else if (items > 40) {
          liEl.style.columnCount = "3";
          liEl.style.width = `${navbar.offsetWidth}px`;
        } else if (items > 30) {
          liEl.style.columnCount = "2";
        } else {
          liEl.style.columnCount = "1";
        }
      }

      menuWidth(submenu, this.navbar);

      window.addEventListener("resize", () => {
        menuWidth(submenu, this.navbar);
      });
    });
  }

  mountMobileNavbar() {
    if (!this.mobileNavbar) return;

    if (this.mobileNavbar.dataset.init) return;
    this.mobileNavbar.dataset.init = true;

    [this.toggleButton] = this.mobileNavbar.getElementsByClassName(
      "uc-navbar_mobile-button"
    );
    const nestedSlides = this.mobileNavbar.querySelectorAll(
      ".uc-navbar_mobile-list .has-list-children"
    );

    nestedSlides.forEach((element) => {
      const listButton = element.getElementsByClassName("list-open")[0];
      listButton.addEventListener("click", () => {
        if (element.getElementsByClassName("list-children").length) {
          element
            .getElementsByClassName("list-children")[0]
            .classList.toggle("is-children-open");
        }
      });
      const closeButtons = element.querySelectorAll(".list-close");
      closeButtons.forEach((buttonsEl) => {
        buttonsEl.addEventListener("click", (close) => {
          close.target.parentNode.parentNode.classList.remove(
            "is-children-open"
          );
        });
      });
    });

    this.toggleButton.addEventListener("click", () => {
      this.mobileNavbar.classList.toggle("is-open");
      const elBody = document.getElementsByTagName("body")[0];
      if (this.mobileNavbar.classList.contains("is-open")) {
        elBody.classList.add("uc-navbar_mobile--open");
      } else {
        elBody.classList.remove("uc-navbar_mobile--open");
      }
      const visibleItems = document.querySelectorAll(".is-children-open");
      visibleItems.forEach((visibleItemsEl) => {
        visibleItemsEl.classList.remove("is-children-open");
      });
    });
  }

  mountNav() {
    this.navElements = this.nav.querySelectorAll("a");

    if (!this.nav) return;

    this.pickDefault();
    this.listenClick();
  }

  // --- Nav fn ---
  listenClick() {
    this.navElements.forEach((el) => {
      el.addEventListener("click", () => {
        this.setActive(el);
      });
    });
  }

  setActive(current) {
    current.classList.add("active");
    this.navElements.forEach((el) => {
      if (el !== current) el.classList.remove("active");
    });
  }

  pickDefault() {
    this.hasDefault = false;

    this.navElements.forEach((el) => {
      if (el.classList.contains("active")) this.hasDefault = true;
    });

    if (!this.hasDefault) this.navElements[0].classList.add("active");
  }
  // --- Nav fn ---
}
