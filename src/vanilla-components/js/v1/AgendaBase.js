import { Card } from "./AgendaActivityCard";

export class Base extends Card {
  constructor(el, uniqueId) {
    super();
    this.element = el;
    this.months = {
      "01": { full: "enero", small: "ene" },
      "02": { full: "febrero", small: "feb" },
      "03": { full: "marzo", small: "mar" },
      "04": { full: "abril", small: "abr" },
      "05": { full: "mayo", small: "may" },
      "06": { full: "junio", small: "jun" },
      "07": { full: "julio", small: "jul" },
      "08": { full: "agosto", small: "ago" },
      "09": { full: "septiembre", small: "sep" },
      10: { full: "octubre", small: "oct" },
      11: { full: "noviembre", small: "nov" },
      12: { full: "diciembre", small: "dic" },
    };
    this.el = el;
    this.uniqueId = uniqueId;

    this.listen();
  }

  mount(getAttrinutes = true) {
    if (getAttrinutes) {
      this.getAttributes();
    }
    this.makeComponent();
  }

  listen() {
    document.addEventListener(
      "reloadAgenda",
      () => {
        this.mount();
      },
      false
    );
  }

  getSkeletonUI(limit = this.limit) {
    let skeletons = '<div class="row" style="padding: 50px 0">';
    for (let index = 0; index < limit; index++) {
      skeletons += `
      <div class="col-sm-${this.sm} col-lg-${this.lg} col-md-${this.md} mb-32">
        <div class="uc-card uc-skeleton">
          ${
            !this.hideImg
              ? '<div class="uc-skeleton-item uc-skeleton_img"></div>'
              : ""
          }
          <div class="uc-card_body">
            <div class="uc-skeleton-item uc-skeleton_span my-20" placeholder="Texto tag">
            </div>
            <div class="uc-skeleton-item uc-skeleton_h4" placeholder="Este es el titulo del card de actividad de tres lineas">
            </div>
            <div class="d-flex mt-32">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
            <div class="d-flex mt-8">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
            <div class="d-flex mt-8">
              <div class="uc-skeleton-item uc-skeleton_span" placeholder="----">
              </div>
              <div class="uc-skeleton-item uc-skeleton_span ml-4" placeholder="item una de lista">
              </div>
            </div>
          </div>
        </div>
      </div>`;
    }
    return `${skeletons}</div>`;
  }

  // eslint-disable-next-line class-methods-use-this
  getErrorUI() {
    return `<div class="uc-message error">
    <div class="uc-message_body">
      <p class="uc-subtitle mb-24">¡Ups!</p>
      <h3 class="mb-16">Algo ha fallado y estamos trabajando en ello.</h3>
      <p class="no-margin">Por favor, intenta más tarde.</p>
    </div>
  </div>`;

    // return `
    //   <div class="row">
    //     <div class="col-lg-12 mb-36">
    //       <div class="uc-card card-height--same card-bg--gray">
    //         <div class="uc-card_body--lg">
    //           <span class="uc-subtitle mb-16">Ha ocurrido un error</span>
    //           <h1 class="mb-16">En este momento, no podemos mostrar eventos, por favor intenta más tarde.</h1>
    //         </div>
    //       </div>
    //     </div>
    //   </div>
    // `;
  }

  // eslint-disable-next-line class-methods-use-this
  getEmptyStateUI() {
    return `
    <div class="uc-message warning">
      <div class="uc-message_body">
        <p class="uc-subtitle mb-24">SIN RESULTADOS</p>
        <h3 class="mb-16">Actualmente no hay actividades para mostrar.<br/> Puede que aún no se hayan publicado para las próximas semanas.</h3>
        <p class="no-margin pb-4">Si quieres ver más eventos y actividades de la UC visita <a href="https://agenda.uc.cl/" target="_blank">agenda.uc.cl</a></p>
        <p class="no-margin">
          En caso de que creas que se trata de un error, puedes contactarte a
          <a href="mailto:agenda@uc.cl">agenda@uc.cl</a>.
        </p>
      </div>
    </div>`;
  }

  getAttributes() {
    const data = this.el.dataset;

    this.dataset = {
      page: data.page ? data.page : 1,
      pageSize: data.limit ? data.limit : 12,
      activityTypes: data.typesOfActivities ? data.typesOfActivities : "",
      organizers: data.organizers ? data.organizers : "",
      keywords: data.keywords ? data.keywords : "",
      place: data.place ? data.place : "",
      targets: data.audience ? data.audience : "",
      from: data.from ? data.from : "",
      to: data.to ? data.to : "",
    };

    this.host = "https://api.agenda.uc.cl";
    this.token = data.token;

    // ! CONTINGENCIA, REDIRIGIR A UC.CL/AGENDA HASTA VALIDAR EL RENDER
    // ! this.base_url = data.baseUrl ? data.baseUrl : "https://uc.cl/agenda/actividad";
    this.base_url = "https://www.uc.cl/agenda/actividad";

    // this.titleContent = data.titleContent ? data.titleContent : ""; TODO: buscar por titulo
    this.limit = data.limit ? data.limit : 12;
    this.sm = 6;
    this.md = 3;
    this.lg = 3;

    this.hideImg = this.el.hasAttribute("hide-img");
    this.hideTag = this.el.hasAttribute("hide-tag");
    this.featured = this.el.hasAttribute("featured");
    this.type = data.type;
    this.middleDate = data.middleDate !== undefined ? "middle-date" : false;
  }

  makeQuery() {
    let query = "populate=*";
    let taxonomyCounter = 0;
    Object.keys(this.dataset).forEach((key) => {
      if (["page", "pageSize", "dateFrom", "dateTo"].includes(key)) return;
      let value = this.dataset[key];

      if (value && value !== "") {
        if (value.includes(",")) value = value.split(",").slice(0, 9);
        else value = [value];
        value.forEach((v, index) => {
          query += `&filters[$or][${taxonomyCounter}][${key}][id][$in][${index}]=${v}`;
          // filters[$or][0][organizers][id][$in][0]=55
        });
        taxonomyCounter++;
      }
    });

    if (this.dataset.from) query += `&dateFrom=${this.dataset.from}`;
    if (this.dataset.to) query += `&dateTo${this.dataset.to}`;
    if (this.page) query += `&page=${this.page}`;
    if (this.pageSize) query += `&pageSize=${this.pageSize}`;
    this.query = query;
  }

  async getData() {
    this.makeQuery();

    let url = `${this.host}/api/activities?${this.query}`;

    if (url.includes("api/api/")) {
      url = url.replace("api/api/", "api/");
    }

    // eslint-disable-next-line no-return-await
    return await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    })
      .then(async (data) => {
        data = await data.json();
        return data;
      })
      .then((data) => {
        this.total = data.meta.pagination.total;
        this.limit = data.meta.pagination.pageSize;
        this.page = data.meta.pagination.page;
        this.max_num_pages = data.meta.pagination.pageCount;

        this.element.setAttribute("data-uc-agenda-fetched", true);
        return data;
      });
  }
}
