export class GlobalTopbar {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.items = [];
    this.hasDropdown = false;
    this.validIds = [
      "uc-global-topbar",
      "uc-global-topbar-en",
      "uc-global-topbar-ucchile",
      // "uc-portal-topbar",
    ];
    this.hideMainLink = this.element.hasAttribute("data-uc-hide-home-link");
    this.initializeTopbarItemsConfig();
    this.isEnglish = this.element.id.includes("en");
  }

  initializeTopbarItemsConfig() {
    // Simplified for demonstration; expand according to actual requirements
    this.items = {
      "uc-global-topbar": [
        {
          title: "Medios",
          isDropdown: true,
          subItems: [
            {
              title: "Medios en la UC",
              url: "https://www.uc.cl/medios-en-la-uc/",
              external: true,
            },
            {
              title: "Radio Beethoven",
              url: "https://www.beethovenfm.cl/",
              external: true,
            },
            {
              title: "Media UC",
              url: "https://media.uc.cl/",
              external: true,
            },
            {
              title: "Revista Universitaria",
              url: "https://revistauniversitaria.uc.cl/",
              external: true,
            },
            {
              title: "Visión",
              url: "https://www.uc.cl/vision-uc/",
              external: true,
            },
          ],
        },
        {
          title: "Biblioteca",
          url: "https://bibliotecas.uc.cl/",
          external: true,
        },
        {
          title: "Donaciones",
          url: "https://donaciones.uc.cl/",
          external: true,
        },
        {
          title: "Mi Portal UC",
          url: "https://portal.uc.cl/",
          external: true,
        },
        {
          title: "Correo",
          isDropdown: true,
          subItems: [
            {
              title: "Mi cuenta UC",
              url: "https://micuenta.uc.cl/",
              external: true,
            },
            // {
            //   title: "Exchange UC",
            //   url: "https://webaccess.uc.cl/",
            //   external: true,
            // },
            {
              title: "Microsoft 365 UC",
              url: "https://outlook.office.com/mail/",
              external: true,
            },
            {
              title: "Gmail UC",
              url: "https://mail.google.com/a/uc.cl",
              external: true,
            },
            {
              title: "Cambio a Microsoft 365",
              url: "https://almacenamientoycorreo.uc.cl/",
              external: true,
            },
          ],
        },
      ],
      "uc-global-topbar-en": [
        {
          title: "Media",
          url: "https://www.uc.cl/medios-en-la-uc/",
          external: true,
          es: true,
        },
        {
          title: "Library",
          url: "https://bibliotecas.uc.cl/",
          external: true,
          es: true,
        },
        {
          title: "Giving",
          url: "https://givingday.uc.cl/",
          external: true,
          es: true,
        },
        {
          title: "Intranet",
          url: "https://portal.uc.cl/",
          external: true,
          es: true,
        },
        {
          title: "E-Mail",
          isDropdown: true,
          subItems: [
            {
              title: "My UC Account",
              url: "https://micuenta.uc.cl/",
              external: true,
              es: true,
            },
            // { 
            //   title: "Exchange UC",
            //   url: "https://webaccess.uc.cl/",
            //   external: true,
            // },
            {
              title: "Gmail UC",
              url: "https://mail.google.com/a/uc.cl",
              external: true,
            },
            {
              title: "Microsoft 365 UC",
              url: "https://outlook.office.com/mail/",
              external: true,
            },
            {
              title: "Change to Microsoft 365",
              url: "https://almacenamientoycorreo.uc.cl/",
              external: true,
            },
          ],
        },
      ],
      "uc-global-topbar-ucchile": [
        {
          title: "Medios",
          isDropdown: true,
          subItems: [
            {
              title: "Medios en la UC",
              url: "https://www.uc.cl/medios-en-la-uc/",
              external: true,
            },
            {
              title: "Radio Beethoven",
              url: "https://www.beethovenfm.cl/",
              external: true,
            },
            {
              title: "Media UC",
              url: "https://media.uc.cl/",
              external: true,
            },
            {
              title: "Revista Universitaria",
              url: "https://revistauniversitaria.uc.cl/",
              external: true,
            },
            {
              title: "Visión",
              url: "https://www.uc.cl/vision-uc/",
              external: true,
            },
          ],
        },
        {
          title: "Biblioteca",
          url: "https://bibliotecas.uc.cl/",
          external: true,
        },
        {
          title: "Donaciones",
          url: "https://donaciones.uc.cl/",
          external: true,
        },
        {
          title: "Mi Portal UC",
          url: "https://portal.uc.cl/",
          external: true,
        },
        {
          title: "Correo",
          isDropdown: true,
          subItems: [
            {
              title: "Mi cuenta UC",
              url: "https://micuenta.uc.cl/",
              external: true,
            },
            // {
            //   title: "Exchange UC",
            //   url: "https://webaccess.uc.cl/",
            //   external: true,
            // },
            {
              title: "Microsoft 365 UC",
              url: "https://outlook.office.com/mail/",
              external: true,
            },
            {
              title: "Gmail UC",
              url: "https://mail.google.com/a/uc.cl",
              external: true,
            },
            {
              title: "Cambio a Microsoft 365",
              url: "https://almacenamientoycorreo.uc.cl/",
              external: true,
            },
          ],
        },
      ],
    };
  }

  async mount() {
    this.id = this.element.id;
    if (!this.validIds.includes(this.id)) return;

    this.items = this.items[this.id] || [];
    this.renderTopbarHTML();
  }

  renderTopbarHTML() {
    let logoUrl;
    if (this.id.includes("portal")) {
      logoUrl = "https://web-uc-prod.s3.amazonaws.com/img/logo-uc-wh.svg";
    } else if (this.id.includes("ucchile") || this.isEnglish) {
      logoUrl =
        "https://kit-digital-uc-prod.s3.amazonaws.com/assets/uc-chile_sm.svg";
    } else {
      logoUrl =
        "https://kit-digital-uc-prod.s3.amazonaws.com/assets/logo-uc-comprimido.svg";
    }

    // check if data-is-agenda-site="true"

    const isAgendaSite = this.element.hasAttribute("data-uc-is-agenda-site");
    const isPortalUc = this.element.hasAttribute("data-uc-is-portal-site");

    let mainLink;
    if (isAgendaSite || isPortalUc)
      if (this.isEnglish)
        mainLink = {
          url: "https://www.uc.cl/en",
          text: "Go to home page",
          external: false,
        };
      else
        mainLink = {
          url: "https://www.uc.cl",
          text: "Ir al inicio",
          external: false,
        };
    else if (this.isEnglish)
      mainLink = {
        url: "https://www.uc.cl/en",
        text: "Go to UC site",
        external: true,
      };
    else
      mainLink = {
        url: "https://www.uc.cl",
        text: "Ir al sitio UC",
        external: true,
      };

    const renderMainLink = (link) => {
      if (this.hideMainLink) return "";
      return `<a href="${link.url}" class="text-size--sm ${
        link.external ? "external" : ""
      }"  target="${link.external ? "_blank" : "_self"}">${link.text}</a>`;
    };

    const admissionText = this.isEnglish ? "Admissions" : "Admisión";
    const admissionUrl = this.isEnglish
      ? "https://admision.uc.cl/informacion-para/international-students/"
      : "https://admision.uc.cl/";

    this.element.innerHTML = `
    <div class="uc-top-bar">
      <div class="container">
        <div class="top-bar_mobile-logo d-block d-lg-none">
          <img src="${logoUrl}" alt="Logo UC" class="img-fluid">
        </div>
        <div class="top-bar_links justify-content-between d-none d-lg-flex">
          <ul class="top-bar_links">
            <li>
              ${renderMainLink(mainLink)}
            </li>
          </ul>
          <ul class="top-bar_links">
            <li class="admission-button-top">
              <a class="text-size--sm external" href="${admissionUrl}" target="_blank">${admissionText}</a>
            </li>
            ${this.renderItems()}
            ${this.renderOtherLanguageSiteUrl()}
          </ul>
        </div>
      </div>
    </div>`;
  }

  renderItems() {
    return this.items.map((item) => this.renderItem(item)).join("");
  }

  renderItem(item) {
    const esFlag = this.isEnglish;
    if (item.isDropdown && item.subItems) {
      this.hasDropdown = true;
      return this.renderDropdownItem(item, esFlag);
    }
    return this.renderSimpleItem(item, esFlag);
  }

  // eslint-disable-next-line class-methods-use-this
  renderSimpleItem(item, esFlag = false) {
    return `<li class="${esFlag && "flag-es"}">
      <a href="${item.url}" target="${
      item.external ? "_blank" : "_self"
    }" class="text-size--sm ${item.external ? "external" : ""}">
        ${item.title}
      </a>
    </li>`;
  }

  // eslint-disable-next-line class-methods-use-this
  renderDropdownItem(item, esFlag = false) {
    return `<li class="${!!esFlag && "flag-es"}">
      <div class="uc-dropdown" style="display:inline-block">
        <button class="uc-btn btn-inline dropbtn text-size--sm p-0" data-dtarget="uc-global-topbar-dropdown-${
          item.title
        }">
          ${
            item.title
          } <i class="uc-icon m-0 color-white icon-size--sm">arrow_drop_down</i>
        </button>
        <div data-dropdown="uc-global-topbar-dropdown-${
          item.title
        }" class="uc-dropdown_list">
          <div class="white-triangle"></div>
          ${item.subItems
            .map(
              (subItem) => `
            <div class="uc-dropdown_list-item px-8 py-12 pt-2">
              <a class="text-size--sm text-strong ${
                subItem.external ? "external" : ""
              } ${subItem.es && "flag-es"}" href="${subItem.url}" target="${
                subItem.external ? "_blank" : "_self"
              }">
                ${subItem.title}
              </a>
            </div>
          `
            )
            .join("")}
        </div>
      </div>
    </li>`;
  }

  renderOtherLanguageSiteUrl() {
    const { ucEnglishUrl: englishUrl, ucSpanishUrl: spanishUrl } =
      this.element.dataset;

    if (englishUrl && spanishUrl) {
      throw new Error(
        "No se puede establecer 'data-uc-english-url' y 'data-uc-spanish-url' al mismo tiempo."
      );
    }

    const url = englishUrl || spanishUrl;
    const language = englishUrl ? "English site" : "Sitio en español";

    if (!url) return "";

    return `
      <li class="ml-8 text-size--sm d-flex">
        <i class="uc-icon icon-size--sm icon-color--white ml-4 mr-4">language</i>
        <a href="${url}" target="_blank" style="margin-top:1px" class="${
      spanishUrl && "flag-es"
    }">${language}</a>
      </li>`;
  }
}
