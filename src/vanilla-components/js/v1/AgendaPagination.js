import { Base } from "./AgendaBase";

export class AgendaPagination extends Base {
  makePagination() {
    this.current_page = this.page ? Number(this.page) : 1;
    let pages = "";

    for (let index = 1; index <= this.max_num_pages; index++) {
      pages += `
        <li class="page-item ${this.current_page === index ? "active" : ""}">
          <a href="#" onclick="window.UCKitDigital.mountedComponents.AgendaPagination['${
            this.uniqueId
          }'].goToPage(${index})" class="page-link">${index}</a>
        </li>
      `;
    }

    return `
      <nav class="uc-pagination">
        <button onclick="window.UCKitDigital.mountedComponents.AgendaPagination['${
          this.uniqueId
        }'].goToPrev()" class="uc-pagination_prev mr-12">
          <i class="uc-icon">keyboard_arrow_left</i>
        </button>
        <ul class="uc-pagination_pages">
          ${pages}
        </ul>
        <button onclick="window.UCKitDigital.mountedComponents.AgendaPagination[${String(
          this.uniqueId
        )}].goToNext()" class="uc-pagination_next ml-12">
          <i class="uc-icon">keyboard_arrow_right</i>
        </button>
      </nav>
    `;
  }

  async goToPrev() {
    if (this.page === 1) {
      return;
    }
    await this.goToPage(this.page - 1);
  }

  async goToNext() {
    if (this.page === this.max_num_pages) {
      return;
    }

    await this.goToPage(this.page + 1);
  }

  async goToPage(index) {
    if (this.page === index) {
      return;
    }

    this.page = index;

    await this.makeComponent();
  }

  async makeComponent() {
    const skeletonUI = this.getSkeletonUI();
    const empyStateUI = this.getEmptyStateUI();
    const errorUi = this.getErrorUI();

    this.el.innerHTML = skeletonUI;
    let data;

    try {
      data = await this.getData();
    } catch (e) {
      this.el.innerHTML = errorUi;
      return;
    }

    if (data.data.length === 0) {
      this.el.innerHTML = empyStateUI;
      return;
    }

    const pagination = this.makePagination();
    let html = "";

    Object.values(data.data).forEach((activity) => {
      html += `
      <div class="col-sm-${this.sm} col-lg-${this.lg} col-md-${this.md} mb-32">
        ${this.makeCard(activity)}
      </div>`;
    });

    this.el.innerHTML = `
      <div id="${this.uniqueId}" class="row">${html}</div>
      ${pagination}
    `;
  }
}
