// eslint-disable-next-line import/prefer-default-export
export class Tabs {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.tabRoot = null;
    this.tabButtons = null;
    this.activeTabs = null;
    this.selectedTab = null;
    this.classList = {
      active: "active",
    };
  }

  mount() {
    this.tabRoot = this.element;
    this.tabButtons = this.tabRoot.querySelectorAll("[data-tabtarget]");
    this.selectableTabs = this.tabRoot.querySelectorAll("[data-tabselect]");

    // Mejora que no se implementó en esta refactorización
    // TODO: evaluar viabilidad de implementar
    // this.tabButtons.forEach((element) => {
    //   if (element.tagName === "A") {
    //     const button = document.createElement("button");
    //     button.innerHTML = element.innerHTML;
    //     button.className = element.className;
    //     const isActive = element.getAttribute("data-tabactive") === "";
    //     if (isActive) button.dataset.tabactive = "";

    //     button.dataset.tabtarget = element.dataset.tabtarget;

    //     button.setAttribute("role", "tab");
    //     element.parentElement.replaceChild(button, element);
    //   }
    // });
    // this.tabButtons = this.tabRoot.querySelectorAll("[data-tabtarget]");
    this.activeTabs = this.tabRoot.querySelectorAll("[data-tabactive]");

    this.tabButtons.forEach((element) => {
      element.addEventListener("click", (e) => this.onClick(e));
    });

    this.selectableTabs.forEach((element) => {
      element.addEventListener("change", (e) => this.onSelect(e));
    });

    this.closeAll();

    this.activeTabs.forEach((element) => {
      const target = this.tabRoot.querySelectorAll(
        `[data-tab="${element.dataset.tabtarget}"]`
      );
      element.classList.add(this.classList.active);

      if (!target) return;
      target.forEach((child) => {
        this.open(child);
      });
    });
  }

  onClick(e) {
    const element = e.target;
    const buttons = this.tabRoot.querySelectorAll("[data-tabtarget]");
    buttons.forEach((button) => {
      button.classList.remove(this.classList.active);
    });
    element.classList.add(this.classList.active);
    this.closeAll(this.tabRoot);
    const targets = this.tabRoot.querySelectorAll(
      `[data-tab="${element.dataset.tabtarget}"]`
    );
    this.action(targets, "open");
  }

  onSelect(e) {
    const element = e.target;

    const selectedtab = element.options[e.target.options.selectedIndex].value;

    this.closeAll(this.tabRoot);

    const targets = this.tabRoot.querySelectorAll(
      `[data-tab="${selectedtab}"]`
    );
    this.action(targets, "open");
  }

  getParent(element) {
    if (
      typeof element.parentElement === "undefined" ||
      element.parentElement == null
    ) {
      return null;
    }
    if (typeof element.parentElement.dataset.tabpanel !== "undefined") {
      return element.parentElement;
    }
    return this.getParent(element.parentElement);
  }

  open(element) {
    this.isopen = true;
    this.isclose = false;
    element.style.display = "";
  }

  close(element) {
    this.isclose = true;
    this.isopen = false;
    element.style.display = "none";
  }

  closeAll() {
    const buttons = this.tabRoot.querySelectorAll("[data-tabtarget]");
    const targets = [];

    buttons.forEach((element) => {
      const objectives = this.tabRoot.querySelectorAll(
        `[data-tab="${element.dataset.tabtarget}"]`
      );
      objectives.forEach((child) => {
        targets.push(child);
      });
    });

    this.action(targets, "close");
  }

  action(targets, action) {
    if (targets == null) {
      return;
    }

    targets.forEach((element) => {
      this[action](element);
    });
  }
}
