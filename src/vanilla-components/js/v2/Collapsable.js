export class Collapsable {
  constructor(element, key) {
    this.key = key;
    this.element = element;
    this.triggerElement = null;
    this.contentElement = null;
    this.collapsableId = null;
    this.contentElementHeight = null;
    this.groupId = null;
    this.animationDuration = 250;
    this.isOpen = false;
    this.element = element;
    this.triggerElement = this.element;
  }

  // get the list of the group of collapsables
  setKeyboardNavigation() {
    if (this.groupId) {
      const currentGroup =
        window.UCKitDigital.componentsSharedState.Collapsable.groups[
          this.groupId
        ];

      // make the focus go to the previous collapsable, if its the first one go to the last
      this.element.addEventListener("keydown", (e) => {
        if (e.key === "ArrowUp" || e.key === "ArrowLeft") {
          e.preventDefault();
          const index = currentGroup.indexOf(this);
          if (index > 0) {
            currentGroup[index - 1].triggerElement.focus();
          } else {
            currentGroup[currentGroup.length - 1].triggerElement.focus();
          }
        }
      });

      this.element.addEventListener("keydown", (e) => {
        if (e.key === "ArrowDown" || e.key === "ArrowRight") {
          e.preventDefault();
          const index = currentGroup.indexOf(this);
          if (index < currentGroup.length - 1) {
            currentGroup[index + 1].triggerElement.focus();
          } else {
            currentGroup[0].triggerElement.focus();
          }
        }
      });
    }
  }

  closeGroup() {
    if (
      window.UCKitDigital?.componentsSharedState?.Collapsable?.groups[
        this.groupId
      ] === undefined
    ) {
      window.UCKitDigital.componentsSharedState = {
        Collapsable: {
          groups: {
            [this.groupId]: [],
          },
        },
      };
    }
    window.UCKitDigital.componentsSharedState.Collapsable.groups[
      this.groupId
    ].push(this);
  }

  closeAllGroupCollapsables() {
    window.UCKitDigital.componentsSharedState.Collapsable.groups[
      this.groupId
    ].forEach((collapsable) => {
      if (collapsable !== this) {
        collapsable.close();
      }
    });
  }

  mount() {
    this.collapsableId = this.triggerElement.dataset.ucCollapsableTriggerFor;
    this.animationDuration =
      this.triggerElement.dataset.ucCollapsableAnimationDuration || 250;
    this.groupId = this.triggerElement.dataset.ucCollapsableCloseGroup;

    if (this.groupId) this.closeGroup();

    this.contentElement = document.querySelector(
      `[data-uc-collapsable-id="${this.collapsableId}"]`
    );

    this.contentElement.setAttribute(
      "id",
      this.element.id || this.collapsableId
    );

    this.contentElement.style.setProperty(
      "--animation-duration",
      `${this.animationDuration}ms`
    );

    if (!this.contentElement) {
      throw new Error("Collapsable: Content element not found.");
    }

    // Initial state
    this.contentElementHeight = this.contentElement.offsetHeight;
    this.contentElement.style.setProperty(
      "--uc-collapsable-height",
      `${this.contentElementHeight}px`
    );
    this.contentElement.dataset.ucCollapsableOpen = "false";

    this.triggerElement.setAttribute("aria-expanded", "false");
    this.triggerElement.setAttribute(
      "aria-controls",
      this.element.id || this.collapsableId
    );

    this.toggleTabIndexes();
    this.triggerElement.addEventListener("click", () => this.toggleOpenState());
    this.setKeyboardNavigation();
  }

  toggleOpenState() {
    this.contentElement.dataset.ucCollapsableOpen = String(!this.isOpen);
    this.triggerElement.setAttribute("aria-expanded", !this.isOpen);

    if (!this.isOpen) {
      if (this.groupId) this.closeAllGroupCollapsables();
      setTimeout(() => {
        if (this.isOpen) this.contentElement.style.overflow = "visible";
      }, this.animationDuration);
    } else {
      this.contentElement.style.overflow = "hidden";
    }
    this.isOpen = !this.isOpen;
    this.toggleTabIndexes();
  }

  close() {
    this.contentElement.dataset.ucCollapsableOpen = "false";
    this.triggerElement.setAttribute("aria-expanded", "false");
    this.contentElement.style.overflow = "hidden";
    this.isOpen = false;
    this.toggleTabIndexes();
  }

  toggleTabIndexes() {
    this.contentElement
      .querySelectorAll("a, button, input, select, textarea")
      .forEach((el) => {
        el.setAttribute("tabindex", this.isOpen ? "0" : "-1");
      });
  }
}
