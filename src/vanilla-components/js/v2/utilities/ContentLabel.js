export class ContentLabel {
  constructor(element, key) {
    this.element = element;
    this.key = key;
    this.contentLabel = this.element.getAttribute("data-uc-content-label");
    this.contentLabelAsSelector = this.contentLabel;
    this.contentLabelElement = null;
  }

  mount() {
    if (!this.findBySelector()) {
      this.createLabelElement();
    }

    this.contentLabelElement.setAttribute("id", this.key);
    this.addAriaLabelledBy();
    this.removeDataAttr();
  }

  findBySelector() {
    try {
      this.contentLabelElement = this.element.querySelector(
        this.contentLabelAsSelector
      );
      return true;
    } catch (error) {
      return false;
    }
  }

  createLabelElement() {
    this.contentLabelElement = document.createElement("label");
    this.contentLabelElement.setAttribute("id", this.key);
    this.contentLabelElement.innerHTML = this.contentLabel;
    this.contentLabelElement.classList.add("uc-sr-only");
    this.element.insertBefore(
      this.contentLabelElement,
      this.element.firstChild
    );
  }

  addAriaLabelledBy() {
    this.element.setAttribute("aria-labelledby", this.key);
  }

  removeDataAttr() {
    this.element.removeAttribute("data-uc-mounted");
    this.element.removeAttribute("data-uc-content-label");
  }
}
