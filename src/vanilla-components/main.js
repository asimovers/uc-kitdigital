import pkg from "../../package.json";

const COMPONENT_DEFINITIONS = {
  ContentLabel: {
    docs: "",
    selectors: ["[data-uc-content-label]"],
    importComponent: async () => {
      const module = await import("./js/v2/utilities/ContentLabel");
      return module.ContentLabel;
    },
  },
  Tooltip_v2: {
    docs: "",
    selectors: [
      "[data-uc-tooltip-content]",
      "[data-uc-tooltip-content-element-id]",
    ],
    importComponent: async () => {
      const module = await import("./js/v2/Tooltip");
      return module.TooltipV2;
    },
  },
  Collapsable: {
    docs: "",
    selectors: ["[data-uc-collapsable-trigger-for]"],
    importComponent: async () => {
      const module = await import("./js/v2/Collapsable");
      return module.Collapsable;
    },
  },
  Accordion: {
    docs: "",
    selectors: ["[data-collapse]", "[data-uc-collapse]"],
    importComponent: async () => {
      const module = await import("./js/v1/Accordion");
      return module.Accordion;
    },
  },
  BackgroundVideo: {
    docs: "",
    selectors: ".uc-background-video",
    importComponent: async () => {
      const module = await import("./js/v1/BackgroundVideo");
      return module.BackgroundVideo;
    },
  },
  Carousel: {
    docs: "",
    selectors: "[data-carousel]",
    importComponent: async () => {
      const module = await import("./js/v1/Carousel");
      return module.Carousel;
    },
  },
  Carousel_v2: {
    docs: "",
    selectors: ".uc-carousel-v2",
    importComponent: async () => {
      const module = await import("./js/v2/Carousel");
      return module.CarouselV2;
    },
  },
  Dropdown: {
    docs: "",
    selectors: ".uc-dropdown",
    importComponent: async () => {
      const module = await import("./js/v1/Dropdown");
      return module.Dropdown;
    },
  },
  Gallery: {
    docs: "",
    selectors: ".uc-gallery",
    importComponent: async () => {
      const module = await import("./js/v1/Gallery");
      return module.Gallery;
    },
  },
  Modal: {
    docs: "",
    selectors: "[data-mtarget]",
    importComponent: async () => {
      const module = await import("./js/v1/Modal");
      return module.Modal;
    },
  },
  Tabs: {
    docs: "",
    selectors: "[data-tabpanel]",
    importComponent: async () => {
      const module = await import("./js/v1/Tabs");
      return module.Tabs;
    },
  },
  Tabs_v2: {
    docs: "",
    selectors: ".uc-tabs-v2",
    importComponent: async () => {
      const module = await import("./js/v2/Tabs");
      return module.TabsV2;
    },
  },
  Tooltip: {
    docs: "",
    selectors: ".uc-tooltip",
    importComponent: async () => {
      const module = await import("./js/v1/Tooltip");
      return module.Tooltip;
    },
  },
  GlobalFooter: {
    docs: "",
    selectors: ["#uc-global-footer", "#uc-global-footer-en"],
    importComponent: async () => {
      const module = await import("./js/v1/GlobalFooter");
      return module.GlobalFooter;
    },
  },
  GlobalTopbar: {
    docs: "",
    selectors: [
      "#uc-global-topbar",
      "#uc-global-topbar-en",
      "#uc-global-topbar-ucchile",
      "#uc-portal-topbar",
    ],
    innerComponnets: ["Dropdown"],
    importComponent: async () => {
      const module = await import("./js/v1/GlobalTopbar");
      return module.GlobalTopbar;
    },
  },
  AgendaRow: {
    docs: "",
    selectors: ".agenda-rows",
    importComponent: async () => {
      const module = await import("./js/v1/AgendaRows");
      return module.AgendaRows;
    },
  },
  AgendaSlider: {
    docs: "",
    selectors: ".agenda-slider",
    innerComponnets: ["Carousel"],
    importComponent: async () => {
      const module = await import("./js/v1/AgendaSlider");
      return module.AgendaSlider;
    },
  },
  AgendaPagination: {
    docs: "",
    selectors: ".agenda-pagination",
    importComponent: async () => {
      const module = await import("./js/v1/AgendaPagination");
      return module.AgendaPagination;
    },
  },
  Navbar: {
    // uc-navbar-dropdown
    docs: "",
    selectors: [".uc-navbar_nav", ".nav", ".navbar_mobile-slide"],
    importComponent: async () => {
      const module = await import("./js/v1/Navbar");
      return module.Navbar;
    },
  },
};

const handleImportAndMountComponent = async (
  importComponent,
  componentName,
  element,
  index
) => {
  const timestamp = +new Date();
  const id = element.getAttribute("id");
  const key = id || `${componentName}-${index}__${timestamp}`;

  const Component = await importComponent();
  let instance = null;
  if (!element.getAttribute("data-uc-mounted")) {
    instance = new Component(element, key);
    instance.mount();
    element.setAttribute("data-uc-mounted", true);
  } else {
    // eslint-disable-next-line no-console
    console.warn(
      `Component ${componentName} with key ${key} is already mounted in the element:`,
      element
    );
  }

  return { key, instance };
};

const handleMountSubComponents = async (component, element) => {
  const isAgendaComponent = [
    "AgendaSlider",
    "AgendaPagination",
    "AgendaRow",
  ].includes(component.componentName);

  const innerComponnets = Array.isArray(component.innerComponnets)
    ? component.innerComponnets
    : [component.innerComponnets];

  const attributeToCheck = isAgendaComponent
    ? "data-uc-agenda-fetched"
    : "data-uc-mounted";

  const checkIfAgendaComponent = () => {
    if (element.getAttribute(attributeToCheck) === "true") {
      innerComponnets.forEach((innerComponentName) => {
        window.UCKitDigital.mountComponent(innerComponentName, element, false);
      });
    } else setTimeout(checkIfAgendaComponent, 100);
  };
  checkIfAgendaComponent();
};

const mountComponent = async (
  componentName,
  elementRoot = document,
  canMountSubcomponents = true
) => {
  const component = COMPONENT_DEFINITIONS[componentName];
  component.componentName = componentName;

  const { importComponent, selectors } = component;
  const elements = elementRoot.querySelectorAll(selectors);

  if (!elements.length) return;

  let instances = await Promise.all(
    Array.from(elements).map(async (element, index) => {
      const instance = handleImportAndMountComponent(
        importComponent,
        componentName,
        element,
        index
      );
      if (component.innerComponnets && canMountSubcomponents)
        handleMountSubComponents(component, element);

      return instance;
    })
  );

  instances = instances.reduce((acc, { key, instance }) => {
    acc[key] = instance;
    return acc;
  }, {});

  window.UCKitDigital.mountedComponents[componentName] = {
    ...window.UCKitDigital.mountedComponents[componentName],
    ...instances,
  };
};

const unMountComponent = (componentName) => {
  const instances = window.UCKitDigital.mountedComponents[componentName];
  if (!instances) return;

  Object.values(instances).forEach((instance) => {
    instance.unMount();
  });

  window.UCKitDigital.mountedComponents[componentName] = {};
};

const mountAllComponents = async () => {
  window.UCKitDigital.componentNames.forEach((componentName) => {
    if (!window.UCKitDigital.options.preventMount.includes(componentName))
      window.UCKitDigital.mountComponent(componentName);
  });
};

const unMountAllComponents = () => {
  window.UCKitDigital.componentNames.forEach((componentName) => {
    window.UCKitDigital.unMountComponent(componentName);
  });
};

const DEFAULT_OPTIONS = {
  preventMount: [
    // "Accordion",
    // "AgendaSlider",
    // "AgendaPagination",
    // "GlobalFooter",
    // "GlobalTopbar",
  ],
};

async function UCKitDigital(options = DEFAULT_OPTIONS) {
  const components = Object.keys(COMPONENT_DEFINITIONS).reduce(
    (acc, componentName) => {
      acc[componentName] = {
        ...COMPONENT_DEFINITIONS[componentName],
      };
      return acc;
    },
    {}
  );

  window.UCKitDigital = {
    version: pkg.version,
    author: pkg.author,
    contributors: pkg.contributors,
    documentation: pkg.documentation,
    // -----------------------------------
    componentNames: Object.keys(COMPONENT_DEFINITIONS),
    componentsSharedState: {},
    mountedComponents: {},
    mountComponent,
    unMountComponent,
    mountAllComponents,
    unMountAllComponents,
    components,
    options,
  };
}

document.addEventListener("DOMContentLoaded", async () => {
  await UCKitDigital();
  window.UCKitDigital.mountAllComponents();
});
