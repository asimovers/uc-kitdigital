import {html, css, LitElement} from 'lit';

export class SimpleGreeting extends LitElement {
  static styles = css`
    p { color: blue }

    /* media query for change p color in mobile */
    @media (max-width: 600px) {
      p { color: red }
    }
  

  `;

  static properties = {
    name: {type: String},
  };

  constructor() {
    super();
    this.name = 'Somebody';
  }

  render() {
    return html`<p>Hello, ${this.name}!</p>`;
  }
}

customElements.define('simple-greeting', SimpleGreeting);
