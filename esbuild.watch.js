/* eslint-disable no-console */

const liveServer = require("@compodoc/live-server");
const esbuild = require("esbuild");
const { sassPlugin } = require("esbuild-sass-plugin");
const postcss = require("postcss");
const autoprefixer = require("autoprefixer");
const { renderLocalViewsWatcher, renderIndex } = require("./buildViews");

liveServer.start({
  port: 3001,
  root: "dist",
  open: false,
  wait: 0,
});

renderIndex(true);
renderLocalViewsWatcher(true);

(async () => {
  const ctxJS = await esbuild.context({
    entryPoints: ["./src/vanilla-components/uc-kitdigital.js"],
    outfile: "./dist/js/uc-kitdigital.js",
    bundle: true,
    format: "iife",
  });

  const ctxCSS = await esbuild.context({
    entryPoints: ["./src/styles/uc-kitdigital.scss"],
    outfile: "./dist/css/uc-kitdigital.css",
    bundle: true,
    plugins: [
      sassPlugin({
        async transform(source) {
          const { css } = await postcss([autoprefixer]).process(source, {
            from: undefined,
          });
          return css;
        },
      }),
    ],
    loader: {
      ".png": "file",
      ".woff": "file",
      ".woff2": "file",
      ".eot": "file",
      ".ttf": "file",
      ".svg": "file",
    },
  });

  await ctxJS.watch();
  await ctxCSS.watch();
  console.log("watching...");
})();
