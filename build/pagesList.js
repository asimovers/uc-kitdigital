const dirTree = require("directory-tree");
const fs = require("fs");

fs.writeFile(
  `pages.json`,
  JSON.stringify(dirTree(`src/vanilla-components/docs`), null, 2),
  (errToWrite) => {
    if (errToWrite) {
      // eslint-disable-next-line no-console
      console.error("Error to write file", errToWrite);
      return false;
    }
    return true;
  }
);
