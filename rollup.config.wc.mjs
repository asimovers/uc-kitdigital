import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';
import { folderInput } from "rollup-plugin-folder-input";

export default {
  input: ['./src/web-components/src/**/*.js'],
  output: {
    format: 'esm',
    dir: './dist',
  },
  plugins: [
    folderInput(),
    resolve(),
    commonjs(),
    terser()
  ]
};

