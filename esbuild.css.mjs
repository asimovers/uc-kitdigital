/* eslint-disable no-console */
/* eslint-disable import/no-extraneous-dependencies */

import esbuild from "esbuild";
import { sassPlugin } from "esbuild-sass-plugin";
import postcss from "postcss";
import autoprefixer from "autoprefixer";

esbuild
  .build({
    logLevel: "info",
    metafile: true,
    assetNames: "[dir]/[name]",
    entryPoints: ["src/styles/uc-kitdigital.scss"],
    outdir: "dist/css",
    bundle: true,
    minify: true,
    target: ["chrome58", "firefox57", "safari11", "edge18"],
    plugins: [
      sassPlugin({
        async transform(source) {
          const { css } = await postcss([autoprefixer]).process(source);
          return css;
        },
      }),
    ],
    loader: {
      ".png": "file",
      ".woff": "file",
      ".woff2": "file",
      ".eot": "file",
      ".ttf": "file",
      ".svg": "file",
    },
  })
  .then(() => console.log("⚡ Build complete! ⚡"))
  .catch(() => process.exit(1));
