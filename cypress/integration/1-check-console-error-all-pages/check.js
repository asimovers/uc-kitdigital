import pages from "../../../pages.json";

const pagesList = pages.children
  .map((page) => page.children.map((p) => p.path))
  .flat();

pagesList.forEach((page) => {
  describe(`Check: ${page}`, () => {
    beforeEach(() => {
      cy.intercept("GET", "/css/uc-kitdigital.css", (req) => {
        req.url = "/dist/css/uc-kitdigital.css";
        req.continue();
      });

      cy.intercept("GET", "/js/uc-kitdigital.js", (req) => {
        req.url = "/dist/js/uc-kitdigital.js";
        req.continue();
      });
    });

    it(`should not have console errors on ${page}`, () => {
      cy.visit(`./dist/${page}`);

      cy.window().then((win) => {
        const consoleErrorSpy = cy.spy(win.console, "error");

        cy.window().should(() => {
          expect(consoleErrorSpy).to.have.callCount(0);
        });
      });
    });
  });
});
