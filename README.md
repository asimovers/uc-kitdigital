# Kit Digital UC v2.0

## Descripción

Libreria de componentes para el desarrollo de aplicaciones web en la Pontificia Universidad Católica de Chile.
Construida en Vanilla JavaScript, basada en Bootstrap 4 (Grilla y Utilidades).

## Instalación

```bash
npm @digitaluc/uc-kitdigital@latest
```

## Uso

La libreria se compone de varios componentes que pueden ser importados de manera individual o en conjunto de manera dinámica mediante el script main.js.

### Importar componentes de manera individual

```javascript

```
