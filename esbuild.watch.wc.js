/* eslint-disable no-console */

const liveServer = require("@compodoc/live-server");
const esbuild = require("esbuild");
const { sassPlugin } = require("esbuild-sass-plugin");
const postcss = require("postcss");
const autoprefixer = require("autoprefixer");
const { renderLocalViewsWatcher, renderIndex } = require("./buildViews");

liveServer.start({
  port: 3001,
  root: "web-components",
  open: false,
  wait: 0,
});

renderIndex(true);
renderLocalViewsWatcher(true);

(async () => {
  const ctxJS = await esbuild.context({
    entryPoints: ["./src/web-components/src/index.js"],
    outfile: "./src/web-components/dist/js/uc-components.js",
    bundle: true,
    format: "esm",
  });

  const ctxCSS = await esbuild.context({
    entryPoints: ["./src/styles/uc-kitdigital.scss"],
    outfile: "./src/web-components/dist/css/uc-kitdigital.css",
    bundle: true,
    plugins: [
      sassPlugin({
        async transform(source) {
          const { css } = await postcss([autoprefixer]).process(source, {
            from: undefined,
          });
          return css;
        },
      }),
    ],
  });

  await ctxJS.watch();
  await ctxCSS.watch();
  console.log("watching...");
})();
